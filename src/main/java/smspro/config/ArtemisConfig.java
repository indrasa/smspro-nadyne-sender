package smspro.config;

import org.apache.activemq.artemis.jms.client.ActiveMQJMSConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Connection;
import javax.jms.Session;

@Configuration
public class ArtemisConfig {

    public static final String QUEUE_SMS_MODEM = "queue-sms-modem";
    public static final String QUEUE_OUTBOX_UPDATE = "queue-outbox-update";
    public static final String QUEUE_LOG_STORE = "queue-store";
    public static final String QUEUE_LOG_STORE_SG = "queue-store-sg";
    public static final String QUEUE_LOG_SEND = "queue-send";

    @Value("${artemis.host}")
    String host;
    @Value("${artemis.port}")
    int port;
    @Value("${artemis.user}")
    String user;
    @Value("${artemis.password}")
    String password;

    @Bean(name = "mqFactory")
    public ActiveMQJMSConnectionFactory getMqFactory() {

        ActiveMQJMSConnectionFactory factory = new ActiveMQJMSConnectionFactory("tcp://"+host+":"+port);
        factory.setUser(user).setPassword(password);
        factory.setReconnectAttempts(-1);
        factory.setThreadPoolMaxSize(100);

        return factory;
    }

    @Bean(name="mqConn")
    public Connection getMqConnection() {
        try {
            Connection conn = getMqFactory().createConnection();
            conn.start();
            return conn;
        } catch (Exception e) {
            return null;
        }
    }

    @Bean(name="mqSession")
    public Session getMqSession() {
        try {
            Session session = getMqConnection().createSession(false, Session.AUTO_ACKNOWLEDGE);

            return session;
        } catch (Exception e) {
            return null;
        }
    }
}
