package smspro.smsc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.lang.invoke.MethodHandles;
import java.util.List;

public class SmscResender implements ApplicationRunner {
    @Autowired
    private NamedParameterJdbcTemplate jdbc1;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments args) throws Exception {
        taskScheduler.scheduleWithFixedDelay(new SmscResenderWorker(), 5000);
    }

    private class SmscResenderWorker implements Runnable {
        @Override
        public void run() {
            try {
                MapSqlParameterSource params = new MapSqlParameterSource();

                String sql = " SELECT o.outbox_id " +
                        " FROM `sms_outbox` o " +
                        " JOIN sms_outbox_status s ON (o.status_id=s.status_id) " +
                        " WHERE 1\n" +
                        " AND status_time >=SUBDATE(NOW(), INTERVAL 1 HOUR) AND status_time < SUBDATE(NOW(), INTERVAL 5 MINUTE) " +
                        " AND status_type = 'fail' " +
                        " AND o.resend_count < 2 ";

                List<Long> rows = jdbc1.queryForList(sql, params, Long.class);

                for (long outboxId : rows) {
                    try {
                        log.info("Resend #"+outboxId);
                        params.addValue("outboxId", outboxId);
                        sql = "CALL recalculateOutboxRef(:refId)";
                        //jdbc1.update(sql, params);
                    } catch (Exception e) {}
                }
            } catch (Exception e) {

            }
        }
    }
}
