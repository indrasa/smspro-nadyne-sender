package smspro.smsc;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.lang.invoke.MethodHandles;
import java.sql.Timestamp;
import java.util.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberToCarrierMapper;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import smspro.Main;
import smspro.component.ConfigUtil;
import smspro.component.MessageUtil;
import smspro.utils.SmscStatus;

@Component
public class SmscTypeNadyne implements ApplicationRunner {
    @Autowired
    private ApplicationContext context;

    @Autowired
    private NamedParameterJdbcTemplate jdbc1;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    ConfigUtil cfg;


    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private HashMap<Long, Map<String,Object>> oldSmscList = new HashMap<Long, Map<String,Object>>();

    @Override
    public void run(ApplicationArguments args) throws Exception {
        HashMap<Long, Map<String,Object>> newSmscList = this.getSmscList();
        List<Long> newIds = new ArrayList<Long>(newSmscList.keySet());
        List<Long> oldIds = new ArrayList<Long>(oldSmscList.keySet());

        log.debug("Found "+newIds.size()+" available SMSC(s)");

        for (int i=0;i<newIds.size();i++) {
            Long smscId = newIds.get(i);

            Map<String,Object> smscCfg = newSmscList.get(smscId);

            SmscTypeNadyneWorker smsc = new SmscTypeNadyneWorker(smscId, smscCfg);
            asyncExecutor.execute(smsc);
        }
    }

    private HashMap<Long, Map<String,Object>> getSmscList() {
        HashMap<Long, Map<String,Object>> smscList = new HashMap<Long, Map<String,Object>>();

        String sql = " SELECT * FROM sms_smsc s INNER JOIN sms_smsc_nadyne n ON (s.smsc_id=n.smsc_id) WHERE smsc_type IN ('bulk','nadyne','infobip') ORDER BY s.smsc_id ";

        List<Map<String, Object>> queryResult = jdbc1.queryForList(sql, (new HashMap<String,Object>()));
        for (int i=0;i<queryResult.size();i++) {
            Map<String,Object> smsc = queryResult.get(i);
            smscList.put(Long.parseUnsignedLong(smsc.get("smsc_id").toString()), smsc);
        }

        return smscList;
    }

    private class SmscTypeNadyneWorker implements Runnable {
        private Long smscId;
        private boolean isRunning = true;
        private Map<String,Object> smscCfg;
        PoolingHttpClientConnectionManager httpConnManager = null;
        CloseableHttpClient httpClient = null;
        boolean dsiFeature = false;

        private final static String TYPE_NADYNE = "bulk";
        private final static String TYPE_BULK = "bulk";
        private final static String TYPE_INFOBIP = "infobip";

        private boolean IS_NADYNE;
        private boolean IS_INFOBIP;

        private String smscType;
        private MessageUtil messageUtil;

        public SmscTypeNadyneWorker (Long smscId, Map<String,Object> smscCfg) throws IOException {
            this.smscId = smscId;


            smscType = ""+smscCfg.get("smsc_type");

            if (smscType.equals(TYPE_INFOBIP)) {
                IS_NADYNE = false;
                IS_INFOBIP = true;
            } else {
                IS_NADYNE = true;
                IS_INFOBIP = false;
            }



            messageUtil = (MessageUtil) context.getBean("messageUtil");

            log.info("Creating instance SMSC #"+smscId+" type: "+smscType);
            this.smscCfg = smscCfg;

        }

        private void init() throws Exception {
            dsiFeature = true;
            log.info("Init connection...");
            log.info("Max connection: "+cfg.get("server.http.sms.max.connection").toString());
            httpConnManager = new PoolingHttpClientConnectionManager();
            httpConnManager.setValidateAfterInactivity(30000);
            httpConnManager.setMaxTotal(10);
            httpConnManager.setDefaultMaxPerRoute(10);

            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    .setSocketTimeout(30000)
                    .setConnectTimeout(30000)
                    .setConnectionRequestTimeout(30000)
                    .build();

            httpClient = HttpClients.custom()
                    .setDefaultRequestConfig(defaultRequestConfig)
                    .setConnectionManager(httpConnManager)
                    .build();
        }

        @Override
        public void run() {
            try {
                init();
                long serviceDelay = 10000;
                int loopCount = 0;
                while(isRunning) {
                    boolean skipDelay = false;
                    try {

                        serviceDelay = Long.parseLong(cfg.getProperty("app.delay.service.smsc"));

                        httpConnManager.setMaxTotal(Integer.parseInt(cfg.get("server.http.sms.max.connection").toString()));

                        if (loopCount%5==0) {
                            log.info("Smsc thread is running, service delay: "+serviceDelay+" ... Connection pool, available connection: "+(httpConnManager.getTotalStats().getMax()-httpConnManager.getTotalStats().getLeased()));
                        }

                        List<Map<String, Object>> msgs = getMessageQueue();

                        for(int i=0;i<msgs.size();i++) {
                            skipDelay = true;
                            if (!isRunning) break;

                            final Map<String, Object> msg = msgs.get(i);
                            processMessage(msg);

                            int pendingConnection = httpConnManager.getTotalStats().getPending();

                            while (pendingConnection > 10) {
                                log.info("Waiting for available connection! Pending: "+pendingConnection+", leased: "+httpConnManager.getTotalStats().getLeased());
                                try {
                                    Thread.sleep(3000);
                                } catch (Exception e) {}
                                pendingConnection = httpConnManager.getTotalStats().getPending();
                            }
                        }
                        msgs.clear();

                    } catch (Exception e) {
                        log.error("Failed to run! "+e,e);
                    }


                    try {
                        if (!skipDelay) Thread.sleep(serviceDelay);
                    } catch (InterruptedException e) { log.error("Sleep error: "+e,e); }
                    loopCount++;
                    if (loopCount%5==0) loopCount = 0;
                }
            } catch (Exception e) {
                log.error("Error running: "+e,e);
            }




            log.info("SMSC #"+smscId+" stopped! ....");
        }

        public void stopInstance() {
            this.isRunning = false;
        }

        private List<Map<String, Object>> getMessageQueue() {
            String sql = "";
            sql = " select t.* FROM sms_outbox_tmp t, user u WHERE u.user_id=t.user_id and sch_time<=NOW() and (hold_time IS NULL OR hold_time<=NOW()) and t.status_id IN (90,93) and suspended=0 ";
            sql+= " AND t.smsc_id=:smsc_id  and u.send_time_start<=TIME(NOW()) AND u.send_time_end>=TIME(NOW()) ";
            sql+= " ORDER BY t.priority, t.hold_time, t.ins_time, t.outbox_id LIMIT 0,:limit ";

            HashMap<String,Object> params = new HashMap<String,Object>();
            params.put("smsc_id", smscId);
            params.put("limit", 500);

            List<Map<String, Object>> queryResult = jdbc1.queryForList(sql,params);
            return queryResult;
        }

        private void processMessage(Map<String, Object> msg) {
            Long outboxId = (Long) msg.get("outbox_id");
            // prevent message being processed 2x
            setMessageHold(outboxId, 30, false);
            log.info("["+outboxId+"] Processing message "+outboxId+" ...");

            if (!msisdnIsValid(msg)) return;
            if (messageIsExpired(msg)) return;

            send(msg);

        }

        private boolean msisdnIsValid(Map<String, Object> msg) {
            Long outboxId = (Long) msg.get("outbox_id");
            String msisdn = (String) msg.get("msisdn");

            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

            PhoneNumber indoNumberProto = null;
            boolean valid = false;

            try {
                indoNumberProto = phoneUtil.parse(msisdn, "ID");
                valid = phoneUtil.isValidNumber(indoNumberProto);
                // System.out.println(phoneUtil.format(indoNumberProto, PhoneNumberFormat.E164));
            } catch (NumberParseException e) {
                valid = false;
                log.error("["+outboxId+"] NumberParseException was thrown: " + e.toString());
            }

            if (!valid) {
                log.warn("["+outboxId+"] Msisdn "+msisdn+" is not valid number!");
                try {
                    messageUtil.setMessageStatus(outboxId, SmscStatus.STATUS_INVALID_MSISDN, true);
                } catch (Exception e) {
                    log.error("["+outboxId+"] Error set message expired: "+e,e);
                }
            }

            return valid;
        }

        private boolean messageIsExpired(Map<String, Object> msg) {
            Long outboxId = (Long) msg.get("outbox_id");

            Calendar expTime = Calendar.getInstance();
            expTime.setTimeInMillis( ((Timestamp) msg.get("exp_time")).getTime() );
            Calendar curTime = Calendar.getInstance();

            int resendCount = (Integer) msg.get("resend_count");

            // check if message is expired
            if ((expTime.getTimeInMillis() < curTime.getTimeInMillis()) || resendCount >= 3) {
                try {
                    log.warn("["+outboxId+"] Message #"+outboxId+" is expired or max resend count exceeded!");
                    messageUtil.setMessageStatus(outboxId, SmscStatus.STATUS_EXPIRED, true);
                } catch (Exception e) {
                    log.error("["+outboxId+"] Error set message expired: "+e,e);
                }
                return true;
            }

            return false;
        }

        public void setMessageHold(Long outboxId, int minutes, boolean addResendCounter) {
            log.info("["+outboxId+"] Hold message #"+outboxId+" for "+minutes+" minutes!");
            HashMap<String, Object> params = new HashMap<String, Object>();
            String sql1 = "";
            if (addResendCounter) {
                sql1 = "UPDATE sms_outbox_tmp set hold_time=ADDDATE(NOW(), INTERVAL :minutes MINUTE), resend_count=resend_count+1 where outbox_id=:outbox_id";
            } else {
                sql1 = "UPDATE sms_outbox_tmp set hold_time=ADDDATE(NOW(), INTERVAL :minutes MINUTE) where outbox_id=:outbox_id";
            }

            params.put("outbox_id", outboxId);
            params.put("minutes", minutes);
            jdbc1.update(sql1, params);
        }

        public void addToNoStatus(long outboxId, String msisdn, String msisdnCarrier, String trxId) {
            log.info("["+outboxId+"] Add message #"+outboxId+" to no status queue!");
            HashMap<String, Object> params = new HashMap<String, Object>();
            String sql1 = " INSERT INTO sms_outbox_nadyne_nostatus (outbox_id,msisdn,operator,trx_id,sent_time) VALUES ";
            sql1+= " (:outbox_id,:msisdn,:carrier,:trx_id,NOW()) ON DUPLICATE KEY UPDATE trx_id=:trx_id, sent_time=NOW() ";
            params.put("outbox_id", outboxId);
            params.put("msisdn", msisdn);
            params.put("carrier", msisdnCarrier);
            params.put("trx_id", trxId);
            jdbc1.update(sql1, params);
        }

        private void send(Map<String, Object> msg) {
            Long outboxId = (Long) msg.get("outbox_id");
            String msisdn = (String) msg.get("msisdn");

            try {
                GetThread gt = new GetThread(httpClient, msg);
                gt.start();
            } catch (Exception e) {
                log.error("Error sending #"+outboxId+", msisdn: "+msisdn+": "+e,e);
            }

            //urlBuilder.setParameter(user, smscC)
        }

        private String buildUrl(Map<String, Object> msg, String carrier) throws Exception {
            boolean oldStyle = Boolean.parseBoolean(cfg.getProperty("nadyne.old.style"));
            if (oldStyle) return buildUrlOld(msg, carrier.toUpperCase().trim());
            else return buildUrlNew(msg);
        }

        private String buildUrlNew(Map<String, Object> msg) throws Exception {
            log.info("["+msg.get("outbox_id")+"] Use new style URL!");

            URIBuilder urlBuilder = new URIBuilder();
            urlBuilder.setScheme(cfg.getProperty("nadyne.protocol"));
            urlBuilder.setHost(cfg.getProperty("nadyne.host"));
            urlBuilder.setPort(Integer.parseInt(cfg.getProperty("nadyne.port")));
            urlBuilder.setPath(cfg.getProperty("nadyne.path"));

            urlBuilder.setParameter(cfg.getProperty("nadyne.param.user"), smscCfg.get("url_generic_user").toString());
            urlBuilder.setParameter(cfg.getProperty("nadyne.param.pass"), smscCfg.get("url_generic_pass").toString());
            urlBuilder.setParameter(cfg.getProperty("nadyne.param.sender"), smscCfg.get("source_addr").toString());
            urlBuilder.setParameter(cfg.getProperty("nadyne.param.msisdn"), formatMsisdnNoPlus(msg.get("msisdn").toString()));
            urlBuilder.setParameter(cfg.getProperty("nadyne.param.message"), msg.get("content").toString());

            return urlBuilder.toString();
        }

        private String buildUrlOld(Map<String, Object> msg, String carrier) throws Exception {
            log.info("["+msg.get("outbox_id")+"] Use old style URL!");
            URIBuilder urlBuilder = null;

            if (carrier.equals("TELKOMSEL")) {
                log.info("["+msg.get("outbox_id")+"] use TELKOMSEL URL!");
                urlBuilder = new URIBuilder(smscCfg.get("url_telkomsel").toString());
                urlBuilder.setParameter(cfg.getProperty("nadyne.param.old.user"), smscCfg.get("url_telkomsel_user").toString());
                urlBuilder.setParameter(cfg.getProperty("nadyne.param.old.pass"), smscCfg.get("url_telkomsel_pass").toString());
            } else {
                urlBuilder = new URIBuilder(smscCfg.get("url_generic").toString());
                urlBuilder.setParameter(cfg.getProperty("nadyne.param.old.user"), smscCfg.get("url_generic_user").toString());
                urlBuilder.setParameter(cfg.getProperty("nadyne.param.old.pass"), smscCfg.get("url_generic_pass").toString());
            }

            urlBuilder.setParameter(cfg.getProperty("nadyne.param.old.sender"), smscCfg.get("source_addr").toString());
            urlBuilder.setParameter(cfg.getProperty("nadyne.param.old.msisdn"), formatMsisdnNoPlus(msg.get("msisdn").toString()));
            urlBuilder.setParameter(cfg.getProperty("nadyne.param.old.message"), msg.get("content").toString());
            urlBuilder.setParameter("fa", "outgoing.main");

            return urlBuilder.toString();
        }

        private String formatMsisdnNoPlus(String msisdn) {
            try {
                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                PhoneNumber phoneNumber = phoneUtil.parse(msisdn, "ID");
                msisdn = phoneUtil.format(phoneNumber, PhoneNumberFormat.E164).replaceFirst("[^0-9]", "");
            } catch (Exception e) {
                log.error("Error parse number "+msisdn+" : "+e,e);
            }
            return msisdn;
        }

        private class GetThread extends Thread {

            private final CloseableHttpClient httpClient;
            private final BasicHttpContext context;
            private HttpGet httpget;
            private final long id;
            private final Map<String, Object> msg;
            private final String msisdn;
            private String msisdnCarrier;
            private String url;

            public GetThread(CloseableHttpClient httpClient, Map<String, Object> msg) {
                this.httpClient = httpClient;
                this.context = new BasicHttpContext();
                this.id = (Long) msg.get("outbox_id");
                this.msg = msg;

                this.msisdn = msg.get("msisdn").toString();

                PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
                PhoneNumber pn;
                try {
                    pn = phoneUtil.parse(msisdn,"ID");
                    PhoneNumberToCarrierMapper carrierMapper = PhoneNumberToCarrierMapper.getInstance();
                    msisdnCarrier = carrierMapper.getNameForNumber(pn, Locale.ENGLISH).toUpperCase().trim();
                } catch (NumberParseException e) {
                    // TODO Auto-generated catch block
                    msisdnCarrier = "UNKNOWN";
                }
            }

            @Override
            public void run() {
                Calendar c1 = Calendar.getInstance();

                try {

                    url = buildUrl(msg, msisdnCarrier);
                    httpget = new HttpGet(url);

                    long availableConnection = httpConnManager.getTotalStats().getMax() - httpConnManager.getTotalStats().getLeased();
                    log.info("["+id+"] Connection pool, available connection: "+availableConnection);
                    log.info("["+id+"] URL: "+url);

                    CloseableHttpResponse response = httpClient.execute(httpget, context);
                    HttpEntity entity = response.getEntity();

                    if (entity != null) {
                        byte[] bytes = EntityUtils.toByteArray(entity);
                        String respText = new String(bytes);
                        processResponse(respText);

                        Double etd = (double) ((Calendar.getInstance().getTimeInMillis()-c1.getTimeInMillis())/1000);
                        String et = String.format("%.3f", etd);
                        log.info("["+id+"] Response (elapsed time: "+et+" sec) #"+id+":\n"+respText);
                    }

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Double etd = (double) ((Calendar.getInstance().getTimeInMillis()-c1.getTimeInMillis())/1000);
                    String et = String.format("%.3f", etd);
                    log.error("["+id+"] Error receiving response! (elapsed time: "+et+" sec) "+e,e);
                    setMessageHold(id, 15, false);
                }
            }

            private void processResponse(String response) {
                XPath xpath;
                try {
                    InputSource source = new InputSource(new StringReader(response));
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document document = db.parse(source);

                    XPathFactory xpathFactory = XPathFactory.newInstance();
                    xpath = xpathFactory.newXPath();

                    String trxId = xpath.evaluate("/message/TrxID", document);
                    String statusText = xpath.evaluate("/message/Status", document);
                    log.info("["+id+"] Status: "+statusText);

                    if (statusText.equals("SENT")) {
                        log.info("["+id+"] Message #"+id+", MSISDN: "+msg.get("msisdn")+" success! :) Status: "+statusText);
                        messageUtil.setMessageSent(id, statusText, trxId);
                        addToNoStatus(id,""+msg.get("msisdn"),msisdnCarrier,trxId);
                    } else if (statusText.toUpperCase().contains("SENT FAILED-NO ROUTE")) {
                        // route to other SMSC
                        if (dsiFeature) {
                            try {
                                sendDsiSmsc(msg);
                            } catch (Exception e) {
                                log.error("Error: "+e,e);
                                messageUtil.setMessageStatus(id, SmscStatus.STATUS_FAILED, true);
                            }
                        } else {
                            messageUtil.setMessageStatus(id, SmscStatus.STATUS_FAILED, true);
                        }
                    } else {
                        //setMessageStatus(id, SmscStatus.STATUS_FAILED, false);
                        setMessageHold(id, 15, true);
                    }

                } catch (Exception e) {
                    log.error("["+id+"] Error processing response! "+e,e);
                }
            }
        }

        public void sendDsiSmsc(Map<String, Object> msg) throws Exception {
            log.info("Sending message via DSI SMSC");
            long id = (Long) msg.get("outbox_id");

            String baseUrl = cfg.getProperty("dsi.2nd.smsc.url");
            if (baseUrl == null) baseUrl = "http://10.1.1.21:18080/sms/smsreguler.php";
            URIBuilder urlBuilder = new URIBuilder(baseUrl);
            urlBuilder.setParameter("number", ""+msg.get("msisdn"));
            urlBuilder.setParameter("message", ""+msg.get("content"));
            String url = urlBuilder.toString();

            log.info("DSI SMSC URL: "+url);

            HttpGet httpget = new HttpGet(url);

            BasicHttpContext context = new BasicHttpContext();
            CloseableHttpResponse response = httpClient.execute(httpget, context);

            HttpEntity entity = response.getEntity();

            if (entity != null) {
                byte[] bytes = EntityUtils.toByteArray(entity);
                String respText = new String(bytes);
                log.info("Response: "+respText);
            }

            if (response.getStatusLine().getStatusCode()>=200 && response.getStatusLine().getStatusCode()<300) {
                messageUtil.setMessageStatus(id, SmscStatus.STATUS_SENT, true);

            } else throw (new Exception("Sending failed!"));
        }


    }
}


