package smspro.smsc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

@Component
public class SmscRecalculateStat implements ApplicationRunner {

    @Autowired
    private NamedParameterJdbcTemplate jdbc1;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments args) throws Exception {
        taskScheduler.scheduleWithFixedDelay(new SmscRecalculateStatWorker(), 5000);
    }

    private class SmscRecalculateStatWorker implements Runnable {
        @Override
        public void run() {
            try {
                MapSqlParameterSource params = new MapSqlParameterSource();
                String sql = " SELECT ref_id FROM sms_outbox_ref_update WHERE next_update_time < NOW()  ";
                List<Long> rows = jdbc1.queryForList(sql, params, Long.class);

                for (long refId : rows) {
                    try {
                        log.info("Recalculate ref ID: "+refId);
                        params.addValue("refId", refId);
                        sql = "CALL recalculateOutboxRef(:refId)";
                        jdbc1.update(sql, params);
                    } catch (Exception e) {}
                }
            } catch (Exception e) {

            }
        }
    }
}
