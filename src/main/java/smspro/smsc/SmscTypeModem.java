package smspro.smsc;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.pool.PoolStats;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import smspro.component.*;
import smspro.config.ArtemisConfig;
import smspro.utils.SmscStatus;

import javax.jms.Session;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;

@Component
public class SmscTypeModem implements ApplicationRunner {
    @Autowired
    ModemUtil modemUtil;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    MessageUtil messageUtil;
    @Autowired
    MqUtil mqUtil;

    private static final ThreadPoolTaskExecutor asyncExecutor;
    static {
        asyncExecutor = new ThreadPoolTaskExecutor();
        asyncExecutor.setMaxPoolSize(20);
        asyncExecutor.setCorePoolSize(5);
        asyncExecutor.setQueueCapacity(5);
        asyncExecutor.setThreadNamePrefix("CONTRACT-AGENT-");
        asyncExecutor.initialize();
    }

    private final static HttpConnectionManager connManager = new HttpConnectionManager(10);

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Starting modem services ....");

        asyncExecutor.execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        (new SmscTypeModemWorker()).run();
                        Thread.sleep(10000);
                    } catch (Exception e) {

                    }
                }
            }
        });

    }

    @JmsListener(destination = ArtemisConfig.QUEUE_SMS_MODEM)
    public void loadLog(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        JSONObject jsonReq = new JSONObject(jsonObjectString);
        log.info("Received request sms: "+jsonReq.toString());

        boolean retry = false;
        long outboxId = jsonReq.getLong("outbox_id");
        do {
            retry = false;
            try {
                Set<HttpRoute> routes = connManager.getHttpConnManager().getRoutes();

                PoolStats stats = connManager.getHttpConnManager().getTotalStats();
                log.info("Leased: "+stats.getLeased()+", pending: "+stats.getPending()+", available: "+stats.getAvailable());

                while (modemUtil.getCurrentQueue() > 1) {
                    log.warn("Modem queue is full! ...");
                    try {
                        Thread.sleep(15000);
                    } catch (Exception e1) {};
                }

                while (asyncExecutor.getActiveCount() > 15) {
                    log.info("Waiting for available thread!");
                    Thread.sleep(3000);
                }
                asyncExecutor.execute(new SmscTypeModemSender(jsonReq));
                Thread.sleep(500);
                retry = false;
            } catch (Exception e) {
                if (e instanceof org.springframework.core.task.TaskRejectedException) {
                    log.error("Throtle limit exceeded, hold and retrying ... ");
                    try {
                        //log.info("Maximum process limit ("+i+"/"+size+") (Queue: "+asyncExecutor.getThreadPoolExecutor().getQueue().size()+", active: "+asyncExecutor.getThreadPoolExecutor().getActiveCount()+")! retrying ...!");
                        Thread.sleep(5000);
                    } catch (Exception e1) {};
                    retry = true;
                } else {
                    try {
                        messageUtil.setMessageStatus((long) outboxId, SmscStatus.STATUS_FAILED, true);
                    } catch (Exception e1) {}
                }
            }
        } while (retry);
    }

    private class SmscTypeModemWorker {
        public void run() {
            log.info("Checking modem messages ... ");

            List<Map<String, Object>> msgs = messageUtil.getMessageQueueModem();
            if (msgs.size() <= 0) return;

            log.info("Found message: "+msgs.size());

            for (Map<String,Object> msg : msgs ){
                JSONObject jsonMsg = new JSONObject(msg);
                log.info("Starting sending: "+jsonMsg.toString());
                try {
                    while (modemUtil.getCurrentQueue() > 3) {
                        log.warn("Modem queue is full! ...");
                        try {
                            Thread.sleep(3000);
                        } catch (Exception e1) {};
                    }

                    messageUtil.setMessageStatus(jsonMsg.getLong("outbox_id"), SmscStatus.STATUS_BEING_PROCESSED, true);
                    mqUtil.sendJson(ArtemisConfig.QUEUE_SMS_MODEM, jsonMsg);
                    try {
                        Thread.sleep(300);
                    } catch (Exception e1) {};
                } catch (Exception e) {};
            }
        }
    }

    private class SmscTypeModemSender implements Runnable {
        final JSONObject jsonMsg;
        private final String msisdn;
        private final String msg;
        private final int outboxId;

        private SmscTypeModemSender(JSONObject jsonMsg) {
            this.jsonMsg = jsonMsg;
            msg = jsonMsg.getString("content");
            msisdn = jsonMsg.getString("msisdn");
            outboxId = jsonMsg.getInt("outbox_id");
        }

        private void send() {
            try {
                modemUtil.send(connManager, outboxId, msisdn, msg, -1);
            } catch (Exception e) {
                log.error("Error sending to modem: "+e,e);
                try {
                    messageUtil.setMessageStatus((long) outboxId, SmscStatus.STATUS_FAILED, true);
                } catch (Exception e1) {}
            }
        }

        @Override
        public void run() {
            send();
        }
    }
}
