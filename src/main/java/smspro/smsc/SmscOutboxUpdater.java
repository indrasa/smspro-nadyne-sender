package smspro.smsc;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import smspro.config.ArtemisConfig;

import javax.jms.Session;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;

@Component
public class SmscOutboxUpdater implements ApplicationRunner {

    @Autowired
    private NamedParameterJdbcTemplate jdbc1;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //taskScheduler.scheduleWithFixedDelay(new SmscOutboxUpdaterWorker(), 3000);
    }


    @JmsListener(destination = ArtemisConfig.QUEUE_OUTBOX_UPDATE)
    public void updateOutbox(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        JSONObject jsonReq = new JSONObject(jsonObjectString);
        try {
            MapSqlParameterSource params = new MapSqlParameterSource();
            log.info("Update outbox #: "+jsonReq.get("outboxId")+", status ID: "+jsonReq.get("statusId"));
            params.addValue("outboxId", jsonReq.get("outboxId"));
            params.addValue("statusId", jsonReq.get("statusId"));
            params.addValue("refId", (jsonReq.has("refId"))?jsonReq.get("refId"):null);
            params.addValue("statusText", (jsonReq.has("statusText"))?jsonReq.get("statusText"):null);

            String sql = "";
            if (jsonReq.getBoolean("isSent")) {
                sql = "UPDATE sms_outbox set smsc_ref_id=:refId, smsc_org_status=:statusText, smsc_org_ref=:refId, status_id=:statusId, sent_time=NOW(), status_time=NOW() where outbox_id=:outboxId";
            } else {
                sql = "UPDATE sms_outbox set status_id=:statusId, status_time=NOW() where outbox_id=:outboxId";
            }
            jdbc1.update(sql, params);
        } catch (Exception e) {
            log.error("Error update outbox: "+e,e);
        }
    }

    private class SmscOutboxUpdaterWorker implements Runnable {
        @Override
        public void run() {
            try {
                MapSqlParameterSource params = new MapSqlParameterSource();
                String sql = " SELECT * FROM sms_outbox_update WHERE 1 ORDER BY id LIMIT 0,100 ";
                List<Map<String,Object>> rows = jdbc1.queryForList(sql, params);

                for (Map<String,Object> row : rows) {
                    try {
                        log.info("Update outbox #: "+row.get("outbox_id")+", status ID: "+row.get("status_id"));
                        params.addValue("outboxId", row.get("outbox_id"));
                        params.addValue("statusId", row.get("status_id"));
                        params.addValue("refId", row.get("smsc_ref_id"));
                        params.addValue("statusText", row.get("smsc_status_text"));
                        if ((long) row.get("is_sent") > 0)
                            sql = "UPDATE sms_outbox set smsc_ref_id=:refId, smsc_org_status=:statusText, smsc_org_ref=:refId, status_id=:statusId, sent_time=NOW(), status_time=NOW() where outbox_id=:outboxId";
                        else
                            sql = "UPDATE sms_outbox set status_id=:statusId, status_time=NOW() where outbox_id=:outboxId";
                        jdbc1.update(sql, params);

                        params.addValue("id", row.get("id"));
                        sql = " DELETE FROM sms_outbox_update WHERE id=:id ";
                        jdbc1.update(sql, params);

                    } catch (Exception e) {
                        log.error("Error update outbox: "+e,e);
                    }
                }
            } catch (Exception e) {

            }
        }
    }
}
