package smspro.smsc;

import org.apache.commons.text.StringSubstitutor;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.config.ConnectionConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import smspro.component.HttpConnectionManager;
import smspro.component.MessageUtil;
import smspro.utils.SmscStatus;

import java.lang.invoke.MethodHandles;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Component
public class SmscTypeNadyne2 implements ApplicationRunner {
    private final static String TYPE_NADYNE2 = "nadyne2";

    @Autowired
    ApplicationContext context;
    @Autowired
    private NamedParameterJdbcTemplate jdbc1;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    MessageUtil msgUtil;

    private static final int POOL_SIZE = 1000;

    private static final int POOL_MIN = POOL_SIZE - Math.round(POOL_SIZE/10F);
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    private static final HttpConnectionManager connManager = new HttpConnectionManager(POOL_SIZE);

    private static final int CONNECTION_TIMEOUT_MS = 60 * 1000; // Timeout in millis.
    private static final RequestConfig requestConfig = RequestConfig.custom()
            .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
            .setConnectTimeout(CONNECTION_TIMEOUT_MS)
            .setSocketTimeout(CONNECTION_TIMEOUT_MS)
            .build();

    private static final ThreadPoolTaskExecutor asyncExecutor;
    static {
        log.info("Pool min: "+POOL_MIN);
        asyncExecutor = new ThreadPoolTaskExecutor();
        asyncExecutor.setMaxPoolSize(POOL_SIZE);
        asyncExecutor.setQueueCapacity(15);
        asyncExecutor.setThreadNamePrefix("CONTRACT-AGENT-");
        asyncExecutor.initialize();

        connManager.getHttpConnManager().setMaxTotal(POOL_SIZE*2);
        connManager.getHttpConnManager().setDefaultMaxPerRoute(POOL_SIZE);
    }





    private List<Map<String, Object>> getMessageQueue() {
        String sql = " select t.*, c.msisdn as sender_id, i.url_generic, r.subject " +
                " FROM sms_outbox_tmp t JOIN user u ON (u.user_id=t.user_id) JOIN sms_smsc c ON (c.smsc_id=t.smsc_id) JOIN sms_smsc_nadyne i ON (i.smsc_id=c.smsc_id) JOIN sms_outbox o ON (o.outbox_id=t.outbox_id) JOIN sms_outbox_ref r ON (r.ref_id=o.ref_id) " +
                " WHERE 1 and t.sch_time<=NOW() and (hold_time IS NULL OR hold_time<=NOW()) and t.status_id IN (90,93) and t.suspended=0 " +
                " AND t.smsc_id IN (SELECT smsc_id FROM sms_smsc WHERE smsc_type=:smscType) and u.send_time_start<=TIME(NOW()) AND u.send_time_end>=TIME(NOW()) " +
                " ORDER BY t.priority, t.hold_time, t.ins_time, t.outbox_id LIMIT 0,:limit ";

        HashMap<String,Object> params = new HashMap<String,Object>();
        params.put("smscType", TYPE_NADYNE2);
        params.put("limit", 1000);

        List<Map<String, Object>> queryResult = jdbc1.queryForList(sql,params);
        return queryResult;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        taskScheduler.scheduleWithFixedDelay(new SmscTypeNadyne2Worker(), 10);
    }

    private class SmscTypeNadyne2Worker implements Runnable {
        @Override
        public void run() {
            try {
                connManager.getHttpConnManager().closeExpiredConnections();
                connManager.getHttpConnManager().closeIdleConnections(60, TimeUnit.SECONDS);

                log.info("Starting Nadyne2 worker, active thread: "+asyncExecutor.getActiveCount());
                List<Map<String, Object>> rows = getMessageQueue();
                if (rows.size()<=0) {
                    Thread.sleep(15000);
                    return;
                }

                log.info("Found "+rows.size()+" queued message(s)");
                for (Map<String,Object> row : rows) {
                    JSONObject jsonMsg = new JSONObject(row);
                    final long outboxId = jsonMsg.getLong("outbox_id");

                    while(connManager.getHttpConnManager().getTotalStats().getPending()> 2) {
                        log.info("Waiting for free connection ...");
                        Thread.sleep(1000);
                    }

                    log.info("HTTP Stats Pending: "+connManager.getHttpConnManager().getTotalStats().getPending()+", available: "+connManager.getHttpConnManager().getTotalStats().getAvailable()+", leased "+connManager.getHttpConnManager().getTotalStats().getLeased());

                    try {
                        while (asyncExecutor.getThreadPoolExecutor().getActiveCount() > POOL_MIN) {
                            log.info("Waiting for available thread!");
                            Thread.sleep(1000);
                        }

                        msgUtil.setMessageStatus(outboxId, SmscStatus.STATUS_BEING_PROCESSED, true);
                        //asyncExecutor.getThreadPoolExecutor().execute(new SmscTypeNadyne2Sender(jsonMsg));
                        //log.info("Active thread: "+asyncExecutor.getThreadPoolExecutor().getActiveCount());
                        asyncExecutor.execute(new SmscTypeNadyne2Sender(jsonMsg));
                        log.info("Active thread: "+asyncExecutor.getActiveCount());
                        //Thread.sleep(10);
                    } catch (Exception e) {
                        msgUtil.setMessageStatus(outboxId, SmscStatus.STATUS_FAILED, true);
                        log.error("Error generate sending: "+e,e);
                    }
                }

            } catch (Exception e) {
                log.error("Error while processing queue: "+e,e);
            }

        }
    }

    private class SmscTypeNadyne2Sender implements Runnable  {
        private final JSONObject jsonMsg;
        private final Long outboxId;

        public SmscTypeNadyne2Sender(JSONObject jsonMsg) {
            this.jsonMsg = jsonMsg;
            outboxId = jsonMsg.getLong("outbox_id");
        }
        public void run() {
            long t1 = System.currentTimeMillis();
            try {
                Map<String,String> params = new HashMap<>();
                params.put("msisdn", URLEncoder.encode(jsonMsg.getString("msisdn"),"UTF-8"));
                params.put("msg", URLEncoder.encode(jsonMsg.getString("content"),"UTF-8"));
                params.put("desc", URLEncoder.encode(jsonMsg.getString("subject"),"UTF-8"));

                log.info("Send to: "+params.get("msisdn")+" ... ");

                StringSubstitutor substitutor = new StringSubstitutor(params);
                String url = substitutor.replace(jsonMsg.getString("url_generic"));
                log.info("URL: "+url);

                HttpGet httpGet = new HttpGet(url);
                httpGet.setConfig(requestConfig);
                CloseableHttpResponse response = connManager.getHttpClient().execute(httpGet);
                String respText = EntityUtils.toString(response.getEntity());
                connManager.closeResponse(response);

                long t2 = System.currentTimeMillis();
                log.info("Response: "+respText+", time: "+String.format("%.2f", (t2-t1)/1000F)+" secs");

                String[] resps = respText.trim().split("\\|");
                String statusMsg = resps[0];
                String trxId = (resps.length>1)?resps[1]:"";

                log.info("Sending status: "+statusMsg);
                if (statusMsg.toUpperCase().trim().equals("SENT")) {
                    try {
                        msgUtil.setMessageSent(outboxId, statusMsg, trxId);
                    } catch (Exception e) {};
                } else {
                    try {
                        msgUtil.setMessageStatus(outboxId, SmscStatus.STATUS_FAILED, true);
                    } catch (Exception e) {};
                }
            } catch (Exception e) {
                try {
                    msgUtil.setMessageStatus(outboxId, SmscStatus.STATUS_FAILED, true);
                } catch (Exception e1) {};
                log.error("Error while sending queue: " + e, e);
            }



        }

    }
}
