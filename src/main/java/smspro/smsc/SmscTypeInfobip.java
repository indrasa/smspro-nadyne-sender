package smspro.smsc;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import smspro.component.HttpUtil;
import smspro.component.MessageUtil;
import smspro.component.MqUtil;
import javax.jms.Session;
import java.lang.invoke.MethodHandles;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static smspro.utils.SmscStatus.STATUS_FAILED;

@Component
public class SmscTypeInfobip implements ApplicationRunner {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    HttpUtil httpUtil;
    @Autowired
    ApplicationContext contet;
    private NamedParameterJdbcTemplate jdbc1;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MessageUtil messageUtil;
    @Autowired
    MqUtil mqUtil;

    private final static String QUEUE_INFOBIP = "smspro-infobip-sender-xxxxx";

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Starting infobip services ....");
        taskScheduler.scheduleWithFixedDelay(new SmscTypeInfobipWorker(), 10000);
    }

    private class SmscTypeInfobipWorker implements Runnable {
        @Override
        public void run() {
            boolean isContinue = false;
            do {
                isContinue = process();
            } while (isContinue);
        }

        private boolean process() {
            log.info("Checking infobip messages ... ");

            List<Map<String, Object>> msgs = messageUtil.getMessageQueueInfobip();
            if (msgs.size() <= 0) return false;

            log.info("Found message: "+msgs.size());

            for (Map<String,Object> msg : msgs ){
                JSONObject jsonMsg = new JSONObject(msg);
                jsonMsg.put("retryCount", 0);
                try {
                    long outboxId = jsonMsg.getLong("outbox_id");

                    if (jsonMsg.getInt("resend_count") > 2) {
                        log.warn("Maximum attempts reached! Drop the message: #"+outboxId);
                        messageUtil.setMessageStatus(outboxId, STATUS_FAILED, true);
                    }

                    messageUtil.setQueueProcessed(outboxId);
                    mqUtil.sendJson(QUEUE_INFOBIP, jsonMsg);
                } catch (Exception e) {
                    log.error("Error processing message queue: "+e,e);
                }
            }
            return true;
        }

    }

    @JmsListener(destination = QUEUE_INFOBIP)
    public void updateOutbox(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
        try {
            log.info("Received queue: "+jsonObjectString);
            JSONObject jsonMsg = new JSONObject(jsonObjectString);
            long outboxId = jsonMsg.getLong("outbox_id");

            log.info("Retry count: "+jsonMsg.getInt("retryCount"));

            boolean retry = false;
            do {
                retry = false;
                try {
                    asyncExecutor.execute(new SmscTypeInfobipSender(jsonMsg));
                } catch (org.springframework.core.task.TaskRejectedException e) {
                    try {
                        log.error("Throtle limit exceeded, hold and retrying ... ");
                        //log.info("Maximum process limit ("+i+"/"+size+") (Queue: "+asyncExecutor.getThreadPoolExecutor().getQueue().size()+", active: "+asyncExecutor.getThreadPoolExecutor().getActiveCount()+")! retrying ...!");
                        Thread.sleep(2000);
                    } catch (Exception e1) {log.error("Error async execution: "+e1,e1);};
                    retry = true;
                } catch (Exception e) {
                    log.error("Error async execution: "+e,e);
                    try {
                        messageUtil.setMessageStatus((long) outboxId, STATUS_FAILED, true);
                    } catch (Exception e1) {
                        log.error("Error update message status: "+e,e);
                    }
                }
            } while (retry);
        } catch (Exception e) {
            log.error("Error processing queue: "+e);
        }
    }

    private class SmscTypeInfobipSender implements Runnable {
        private final JSONObject msg;
        private final Long outboxId;

        public SmscTypeInfobipSender(JSONObject msg) {
            this.msg = msg;
            outboxId = msg.getLong("outbox_id");
        }

        @Override
        public void run() {
            log.info("Sending msg #"+outboxId+", active pool size: "+asyncExecutor.getActiveCount()+", maximum pool: "+asyncExecutor.getMaxPoolSize());

            try {
                String url = msg.getString("url");
                log.info("Post JSON to: " + url);
                HttpPost httpPost = new HttpPost(url);

                JSONObject jsonReq = new JSONObject();
                String msisdn = msg.getString("msisdn").replaceAll("[^0-9]","");
                jsonReq.put("from", msg.getString("sender_id"));
                jsonReq.put("to", msisdn);
                jsonReq.put("text", msg.getString("content"));

                httpPost.setHeader("Content-Type", "application/json");
                httpPost.setHeader("Authorization", msg.getString("auth"));
                httpPost.setEntity(new StringEntity(jsonReq.toString(4)));

                CloseableHttpResponse response = httpUtil.getHttpClient().execute(httpPost);

                HttpEntity entity = response.getEntity();
                byte[] bytes = EntityUtils.toByteArray(entity);
                String respText = new String(bytes);
                EntityUtils.consume(response.getEntity());
                log.info("Post JSON response: " + respText);

                JSONObject jsonResultRaw = new JSONObject(respText);
                String statusName = jsonResultRaw.getJSONArray("messages").getJSONObject(0).getJSONObject("status").getString("name");
                log.info("Result: " + statusName);

                if (statusName.contains("ACCEPTED") || statusName.startsWith("PENDING_")) {
                    try {
                        String trxId = jsonResultRaw.getJSONArray("messages").getJSONObject(0).getString("messageId");
                        messageUtil.setMessageSent(outboxId, statusName, trxId);
                    } catch (Exception e) {
                    }
                    ;
                } else {
                    try {
                        messageUtil.setMessageStatus(outboxId, STATUS_FAILED, true);
                    } catch (Exception e) {}
                }
            } catch (org.apache.http.NoHttpResponseException | java.net.SocketTimeoutException | UnknownHostException | java.net.ConnectException e) {
                try {
                    messageUtil.setResend(outboxId);
                } catch (Exception e1) {
                    log.error("Error updating status: "+e1,e1);
                };
            } catch (Exception e) {
                log.error("Error sending: "+e,e);
                try {
                    messageUtil.setMessageStatus(outboxId, STATUS_FAILED, true);
                } catch (Exception e1) {
                    log.error("Error updating status: "+e1,e1);
                };
            }
        }
    }
}
