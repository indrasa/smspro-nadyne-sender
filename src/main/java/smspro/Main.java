package smspro;

import java.io.File;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.context.ApplicationContext;

import com.google.i18n.phonenumbers.NumberParseException;

@SpringBootApplication(scanBasePackages = {"smspro"})
public class Main {

	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	public static String DIR = "";


	public static void main(String[] args) throws IOException, NumberParseException {

		try {
			System.setProperty("server.servlet.context-path", "/");
			ApplicationContext context = SpringApplication.run(Main.class, args);
		} catch (Exception e) {
			log.error("Error: "+e,e);
		}
	}


}
