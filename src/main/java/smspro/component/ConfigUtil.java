package smspro.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component("cfg")
@PropertySource("classpath:nadyne.properties")
public class ConfigUtil {
    @Autowired
    private Environment env;

    public String getProperty(String key) {
        return env.getProperty(key);
    }

    public String get(String key) {
        return getProperty(key);
    }
}
