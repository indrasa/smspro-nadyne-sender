package smspro.component;

import org.apache.activemq.artemis.jms.client.ActiveMQDestination;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.*;
import java.lang.invoke.MethodHandles;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

@Component
public class MqUtil {


    public static final String QUEUE_TEST = "gd-queue-test1";
    public static final String QUEUE_MAIL = "gd-queue-mailer-xxx";
    public static final String QUEUE_LOAN_VERIFICATION_1 = "gd-queue-loan-request-verification-1";
    public static final String QUEUE_LOAN_STEP_01 = "gd-queue-loan-request-approval-01-parsing-data";
    public static final String QUEUE_LOAN_STEP_02 = "gd-queue-loan-request-approval-02-deduplication";
    public static final String QUEUE_LOAN_STEP_99 = "gd-queue-loan-request-approval-99-approval";
    public static final String QUEUE_CONTRACT_RECALCULATE = "gd-queue-contract-recalculation";
    public static final String QUEUE_SMS_INFOBIP = "gd-queue-sms-infobip";
    public static final String QUEUE_LOAN_UPDATER = "gd-loan-updater";
    public static final String QUEUE_DOWNLOAD_CIMB = "gd-download-cimb-disbursement";
    public static final String QUEUE_CREATE_AND_SEND_CONTRACT = "gd-create-and-send-contract";

    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    Session mqSession;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public String sendText(String queueName, final String msgString, final long delay, boolean needReply) throws Exception {
        String correlationId = queueName+"-"+ UUID.randomUUID().toString().toUpperCase();

        MessageCreator msgCreator = new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                Message qMsg = session.createTextMessage(msgString);
                qMsg.setJMSDeliveryTime(System.currentTimeMillis()+delay);
                qMsg.setJMSCorrelationID(correlationId);
                qMsg.setBooleanProperty("needReply", needReply);
                return qMsg;
            }
        };
        jmsTemplate.setTimeToLive(86400000);
        jmsTemplate.send(queueName, msgCreator);

        return correlationId;
    }

    public String sendText(String queueName, final String msgString) throws Exception {
        return sendText(queueName, msgString, 0, false);
    }

    public String sendJson(String queueName, JSONObject jsonObject, final long delay, boolean needReply) throws Exception {
        String msgString = jsonObject.toString();
        return sendText(queueName, msgString, delay, needReply);
    }

    public String sendJson(String queueName, JSONObject jsonObject) throws Exception {
        String msgString = jsonObject.toString();
        return sendText(queueName, msgString, 0, false);
    }

    public String createReceiver(String queueName, int timeOut) throws Exception {
        log.info("Creating new receiver "+queueName+", time out: "+timeOut+" seconds, thread name: "+Thread.currentThread().getName());
        try {
            final BlockingQueue<String> response = new ArrayBlockingQueue<>(1);
            Destination destination = ActiveMQDestination.fromPrefixedName("queue://"+queueName);
            MessageConsumer consumer = mqSession.createConsumer(destination);
            consumer.setMessageListener(new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        log.info("Receive message: "+message.getBody(String.class));
                        response.offer(message.getBody(String.class));
                    } catch (JMSException e) {
                        log.error("Error receive message: "+e,e);
                    }
                }
            });

            String result = null;
            if (timeOut < 0) result = response.poll();
            else result = response.poll(timeOut, TimeUnit.SECONDS);
            consumer.close();
            return result;
        } catch (Exception e) {
            log.error("Error : "+e,e);
            return null;
        }
    }

    public String createReceiver(String queueName) throws Exception {
        return createReceiver(queueName, -1);
    }

    public JSONObject createJsonReceiver(String queueName, int timeOut) throws Exception {
        String respText = createReceiver(queueName, timeOut);
        JSONObject jsonResp = new JSONObject(respText);
        return jsonResp;
    }
}
