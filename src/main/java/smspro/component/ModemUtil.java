package smspro.component;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import smspro.utils.SmscStatus;

import java.lang.invoke.MethodHandles;
import java.nio.charset.StandardCharsets;

@Component
@PropertySource("classpath:modem.properties")
public class ModemUtil {
    public static final int STATUS_OK = 200;
    public static final int STATUS_MESSAGE_SENT = 201;
    public static final int STATUS_MESSAGE_DELIVERED = 200;
    public static final int STATUS_INTERNAL_SERVER_ERROR = 500;
    public static final int STATUS_INVALID_MOBILE = 401;
    public static final int STATUS_INVALID_REQUEST = 400;
    public static final int STATUS_SMS_REPEATED = 402;
    public static final int STATUS_FORBIDDEN = 403;
    public static final int STATUS_NOT_FOUND = 404;
    public static final int STATUS_QUEUE_EXCEEDED = 429;
    public static final int STATUS_SMS_SENT_FAILED = 501;
    public static final int STATUS_UNKNOWN = 600;

    public static final String GW_STATUS_SENT = "SENT_OK";
    public static final String GW_STATUS_DELIVERED = "DELIVERED";
    public static final String GW_STATUS_FAILED = "FAILED";

    @Autowired
    private Environment env;
    @Autowired
    private MessageUtil messageUtil;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    String gsmUser;
    String gsmPass;
    String gsmHost;

    @Autowired
    public void init() {
        gsmUser = env.getProperty("gsm.user");
        gsmPass = env.getProperty("gsm.pass");
        gsmHost = env.getProperty("gsm.host");
    }

    private URIBuilder getUrlBuilder(String path) {
        final String gsmHost = env.getProperty("gsm.host");
        final int gsmPort = Integer.parseInt(env.getProperty("gsm.port"));

        URIBuilder builder = new URIBuilder();
        builder.setHost(gsmHost);
        builder.setScheme("http");
        builder.setPort(gsmPort);
        builder.setPath(path);
        return builder;
    }

    private String getUrl(String path) throws Exception {
        final String gsmHost = env.getProperty("gsm.host");
        final int gsmPort = Integer.parseInt(env.getProperty("gsm.port"));

        URIBuilder builder = new URIBuilder();
        builder.setHost(gsmHost);
        builder.setScheme("http");
        builder.setPort(gsmPort);
        builder.setPath(path);
        return builder.build().toString();
    }

    public JSONObject send(HttpConnectionManager connManager, String msisdn, String text) throws Exception {
        return send(connManager, 0, msisdn, text, -1);
    }

    public int getCurrentQueue() throws Exception {
        URIBuilder builder = getUrlBuilder("/api/query_sms_in_queue");
        String url = builder.build().toString();
        HttpResponse<String> httpResponse = requestGet(url);
        JSONObject jsonResp = new JSONObject(httpResponse.getBody());
        return jsonResp.getInt("in_queue");
    }

    public JSONObject send(HttpConnectionManager connManager,int smsId, String msisdn, String text, int portIndex) throws Exception {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumber = phoneUtil.parse(msisdn, "ID");
        msisdn = phoneUtil.format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164);

        String url = getUrl("/api/send_sms");
        log.info("#"+smsId+" sending to : "+msisdn+", text: "+text+", URL: "+url);

        String[] gsmPortsString = env.getProperty("gsm.ports").split(",");


        if (portIndex >=0) {
            String portStr = gsmPortsString[portIndex];
            gsmPortsString = new String[] { portStr };
        }

        int[] ports = new int[gsmPortsString.length];
        for (int i = 0; i < gsmPortsString.length; i++) {
            ports[i] = Integer.parseInt(gsmPortsString[i]);
        }

        JSONArray bodyParam = new JSONArray();
        bodyParam.put((new JSONObject()).put("number", msisdn).put("user_id", smsId));

        JSONObject body = new JSONObject();
        body.put("text", text).put("param", bodyParam).put("port", ports);

        String response = requestPost(connManager, url,body);
        log.info("#"+smsId+"  response: "+response);

        int recheckCount = 0;
        String status = "SENDING";

        JSONObject jsonResult = null;
        do {
            if (recheckCount > 30) throw new Exception("Sending timed out!");
            jsonResult = getRealTimeStatus(connManager, smsId, msisdn);

            if (jsonResult!=null)
                status = jsonResult.getString("status");

            if (jsonResult==null || status.equals("SENDING")) {
                log.info("#"+smsId+" waiting for sending ....");
                Thread.sleep(4000);
            }
            recheckCount++;
        } while (jsonResult==null || status.equals("SENDING"));


        if (status.equals(GW_STATUS_SENT) || status.equals(GW_STATUS_DELIVERED)) {
            messageUtil.setMessageStatus((long) smsId, SmscStatus.STATUS_SENT, true);
            jsonResult.put("success", true);
        } else {
            jsonResult.put("success", false);
            messageUtil.setMessageStatus((long) smsId, SmscStatus.STATUS_FAILED, true);
        }

        return jsonResult;
    }

    private JSONObject getRealTimeStatus0(HttpConnectionManager connManager, int smsId, String msisdn) throws Exception {
        String url = getUrl("/api/query_sms_result");
        JSONObject body = new JSONObject();
        body.put("number", (new JSONArray()).put(msisdn));
        body.put("user_id", (new JSONArray()).put(smsId));
        String response = requestPost(connManager, url,body);
        log.info("#"+smsId+" response: "+response);

        JSONObject jsonResult = new JSONObject(response);
        return jsonResult;
    }

    public JSONObject getRealTimeStatus(HttpConnectionManager connManager, int smsId, String msisdn) throws Exception {
        JSONObject jsonResult = getRealTimeStatus0(connManager, smsId, msisdn);
        JSONArray arrayResult = jsonResult.getJSONArray("result");
        if (arrayResult.length()>0) {
            arrayResult.getJSONObject(0).put("error_code", jsonResult.getInt("error_code"));
            return arrayResult.getJSONObject(0);
        }

        else return null;
    }

    private String requestPost(HttpConnectionManager connManager, String url, JSONObject body) throws Exception {

        HttpPost httpPost = new HttpPost(url);
        StringEntity entity = new StringEntity(body.toString());
        httpPost.setEntity(entity);

        httpPost.setHeader("Content-type", "application/json");
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(gsmUser, gsmPass));
        AuthCache authCache = new BasicAuthCache();
        HttpHost httpHost = new HttpHost(gsmHost);
        authCache.put(httpHost, new BasicScheme());

        final HttpClientContext context = HttpClientContext.create();
        context.setCredentialsProvider(credsProvider);
        context.setAuthCache(authCache);

        CloseableHttpResponse response = connManager.getHttpClient().execute(httpPost, context);
        String text = EntityUtils.toString(response.getEntity());

        connManager.closeResponse(response);

        return text;
    }

    private HttpResponse<String> requestGet(String url) throws UnirestException {

        HttpResponse<String> response = Unirest
                .get(url)
                .header("Content-type", "application/json")
                .basicAuth(gsmUser, gsmPass)
                .asString();
        return response;

    }

    public JSONArray getReceivedMessage(String flag) throws Exception {
        URIBuilder builder = getUrlBuilder("/api/query_incoming_sms");
        builder.addParameter("flag", flag);
        String url = builder.build().toString();

        HttpResponse<String> response = requestGet(url);
        JSONObject jsonResponse = new JSONObject(response.getBody());
        return jsonResponse.getJSONArray("sms");
    }
}
