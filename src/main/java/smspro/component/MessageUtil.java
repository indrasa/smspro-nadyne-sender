package smspro.component;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;
import smspro.config.ArtemisConfig;
import smspro.utils.SmscStatus;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static smspro.utils.SmscStatus.STATUS_BEING_PROCESSED;
import static smspro.utils.SmscStatus.STATUS_SCHEDULED_FOR_RESEND;

@Component
public class MessageUtil {
    @Autowired
    private NamedParameterJdbcTemplate jdbc1;
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;
    @Autowired
    ThreadPoolTaskExecutor asyncExecutor;
    @Autowired
    MqUtil mqUtil;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public List<Map<String, Object>> getMessageQueueInfobip() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("limit", 100);

        String sql = " select t.*, c.msisdn as sender_id, i.url, i.auth " +
                " FROM sms_outbox_tmp t JOIN user u ON (u.user_id=t.user_id) JOIN sms_smsc c ON (c.smsc_id=t.smsc_id) JOIN sms_smsc_infobip i ON (i.smsc_id=c.smsc_id) " +
                " WHERE 1 and sch_time<=NOW() and (hold_time IS NULL OR hold_time<=NOW()) and t.status_id IN (90,93) and suspended=0 " +
                " AND t.smsc_id IN (SELECT smsc_id FROM sms_smsc WHERE smsc_type='infobip') and u.send_time_start<=TIME(NOW()) AND u.send_time_end>=TIME(NOW()) " +
                " ORDER BY t.priority, t.hold_time, t.ins_time, t.outbox_id LIMIT 0,:limit ";

        List<Map<String, Object>> queryResult = jdbc1.queryForList(sql,params);
        return queryResult;
    }

    public List<Map<String, Object>> getMessageQueueModem() {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("limit", 50);

        String sql = " select t.*, c.msisdn as sender_id " +
                " FROM sms_outbox_tmp t JOIN user u ON (u.user_id=t.user_id) JOIN sms_smsc c ON (c.smsc_id=t.smsc_id) " +
                " WHERE 1 and sch_time<=NOW() and (hold_time IS NULL OR hold_time<=NOW()) and t.status_id IN (90,93) and suspended=0 " +
                " AND t.smsc_id IN (SELECT smsc_id FROM sms_smsc WHERE smsc_type='modem') and u.send_time_start<=TIME(NOW()) AND u.send_time_end>=TIME(NOW()) " +
                " ORDER BY t.priority, t.hold_time, t.ins_time, t.outbox_id LIMIT 0,:limit ";

        List<Map<String, Object>> queryResult = jdbc1.queryForList(sql,params);
        return queryResult;
    }

    public void setMessageSent(Long outboxId, String statusText, String refId) throws Exception {
        int statusId = SmscStatus.STATUS_SENT;
        boolean deleteFromQueue = true;

        JSONObject jsonReq = new JSONObject();
        jsonReq.put("outboxId", outboxId);
        jsonReq.put("statusId", statusId);
        jsonReq.put("refId", refId);
        jsonReq.put("statusText", statusText);
        jsonReq.put("isSent", true);

        mqUtil.sendJson(ArtemisConfig.QUEUE_OUTBOX_UPDATE, jsonReq);

        HashMap<String, Object> params = new HashMap<String, Object>();
        String sql2 = "DELETE FROM sms_outbox_tmp where outbox_id=:outbox_id";
        params.clear();
        params.put("status_id", statusId);
        params.put("outbox_id", outboxId);
        params.put("statusText", statusText);
        params.put("refId", refId);

        if (deleteFromQueue)
            jdbc1.update(sql2, params);
    }

    public void setQueueProcessed(Long outboxId) {
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("statusId", STATUS_BEING_PROCESSED);
        sqlParams.addValue("outboxId", outboxId);

        String sql = " UPDATE sms_outbox_tmp SET status_id=:statusId WHERE outbox_id=:outboxId  ";
        jdbc1.update(sql, sqlParams);
        sql = " UPDATE sms_outbox SET status_id=:statusId WHERE outbox_id=:outboxId  ";
        jdbc1.update(sql, sqlParams);

    }

    public void setResend(Long outboxId) {
        log.info("Resend message #"+outboxId);
        MapSqlParameterSource sqlParams = new MapSqlParameterSource();
        sqlParams.addValue("statusId", STATUS_SCHEDULED_FOR_RESEND);
        sqlParams.addValue("outboxId", outboxId);

        String sql = " UPDATE sms_outbox_tmp SET status_id=:statusId, hold_time=ADDDATE(NOW(), INTERVAL 30 SECOND), resend_count=resend_count+1 WHERE outbox_id=:outboxId  ";
        jdbc1.update(sql, sqlParams);
        sql = " UPDATE sms_outbox SET status_id=:statusId WHERE outbox_id=:outboxId  ";
        jdbc1.update(sql, sqlParams);

    }

    public void setMessageStatus(Long outboxId, int statusId, boolean deleteFromQueue) throws Exception {
        JSONObject jsonReq = new JSONObject();
        jsonReq.put("outboxId", outboxId);
        jsonReq.put("statusId", statusId);
        jsonReq.put("isSent", false);

        mqUtil.sendJson(ArtemisConfig.QUEUE_OUTBOX_UPDATE, jsonReq);

        HashMap<String, Object> params = new HashMap<String, Object>();
        String sql2 = "DELETE FROM sms_outbox_tmp where outbox_id=:outbox_id";
        params.clear();
        params.put("status_id", statusId);
        params.put("outbox_id", outboxId);
        if (deleteFromQueue)
            jdbc1.update(sql2, params);

    }

}
