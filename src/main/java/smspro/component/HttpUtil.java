package smspro.component;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.invoke.MethodHandles;

@Component
public class HttpUtil {
    private static PoolingHttpClientConnectionManager httpConnManager;
    private static CloseableHttpClient httpClient;
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    static  {
        try {
            log.info("Init HTTP client ... ");
            httpConnManager = new PoolingHttpClientConnectionManager();
            httpConnManager.setMaxTotal(300);
            httpConnManager.setDefaultMaxPerRoute(150);
            httpConnManager.setValidateAfterInactivity(30000);

            RequestConfig defaultRequestConfig = RequestConfig.custom()
                    .setSocketTimeout(180000)
                    .setConnectTimeout(180000)
                    .setConnectionRequestTimeout(180000)
                    .build();

            HttpClientBuilder builder = HttpClients.custom()
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .setDefaultRequestConfig(defaultRequestConfig)
                    .setConnectionManager(httpConnManager);

            httpClient = builder.build();
        } catch (Exception e) {
            log.error("Error: "+e,e);
            throw new ExceptionInInitializerError(e);
        }
    }

    public PoolingHttpClientConnectionManager getHttpConnManager() {
        return httpConnManager;
    }

    public CloseableHttpClient getHttpClient() {
        return httpClient;
    }
}
