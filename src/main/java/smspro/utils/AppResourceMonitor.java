package smspro.utils;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppResourceMonitor implements Runnable {
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public AppResourceMonitor() throws IOException {
	}
	
	@Override
	public void run() {
		Runtime rt = Runtime.getRuntime();
		DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
		DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
		symbols.setGroupingSeparator('.');
		
		while (true) {
			long MB = 1048576;
			MB = 1000000;
			long total = rt.totalMemory() / MB;
			long free = rt.freeMemory() / MB;
			long usage = total - free;
			
			log.info("====================================");
			log.info("Total memory:\t"+formatter.format(total)+" MB");
			log.info("Used memory:\t"+formatter.format(usage)+" MB");
			log.info("Free memory:\t"+formatter.format(free)+" MB\n");
			
			try {
				Thread.sleep(15000);
			} catch (InterruptedException e) { log.error("InterruptedException: "+e); }
		}
	}

}
