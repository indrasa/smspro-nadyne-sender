package smspro.utils;

public class SmscStatus {
	public static final int STATUS_EXPIRED = 98;
	public static final int STATUS_SUCCESS = 4;
	public static final int STATUS_SENT = 4;
	public static final int STATUS_DELIVERED = 1;
	public static final int STATUS_UNDELIVERED = 2;
	public static final int STATUS_FAILED = 16;
	public static final int STATUS_SCHEDULED_FOR_RESEND = 93;
	public static final int STATUS_BEING_PROCESSED = 91;
	public static final int STATUS_INVALID_MSISDN = 61;
	public static final int STATUS_ERROR_CONNECTION = 72;
}
