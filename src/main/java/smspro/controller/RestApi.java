package smspro.controller;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import smspro.component.MqUtil;
import smspro.config.ArtemisConfig;

import javax.servlet.http.HttpServletRequest;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.Map;


@RestController
public class RestApi {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    @Autowired
    MqUtil mqUtil;
    @Autowired
    NamedParameterJdbcTemplate jdbc1;

    @RequestMapping(value = {"/send/modem"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> deduction(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        try {
            JSONObject jsonReq = new JSONObject();
            jsonReq.put("msisdn", request.getParameter("mobile").trim());
            jsonReq.put("content", request.getParameter("msg").trim());
            mqUtil.sendJson(ArtemisConfig.QUEUE_SMS_MODEM, jsonReq);
        } catch (Exception e) {
            jsonResult.put("success", false);
            jsonResult.put("msg", e.getMessage());
        }

        return defaultReturn(jsonResult);
        //return new ResponseEntity<String>("ANJENK", new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = {"/nadyne/dlr"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> dlr(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        try {
            JSONObject jsonReq = new JSONObject();
            jsonReq.put("msisdn", request.getParameter("mobile").trim());
            jsonReq.put("content", request.getParameter("msg").trim());
            mqUtil.sendJson(ArtemisConfig.QUEUE_SMS_MODEM, jsonReq);
        } catch (Exception e) {
            jsonResult.put("success", false);
            jsonResult.put("msg", e.getMessage());
        }

        return defaultReturn(jsonResult);
        //return new ResponseEntity<String>("ANJENK", new HttpHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = {"/nadyne/detail/ref/{id}"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> detail(HttpServletRequest request, @PathVariable String id) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", false);

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", id);

        String sql = " SELECT o.*,r.subject FROM sms_outbox o JOIN sms_outbox_ref r ON (o.ref_id=r.ref_id) WHERE smsc_ref_id=:id ";
        List<Map<String,Object>> rows = jdbc1.queryForList(sql, params);
        if (rows.size() > 0) {
            jsonResult.put("success", true);
            jsonResult.put("data", rows.get(0));
        }

        return defaultReturn(jsonResult);
    }

    private ResponseEntity defaultReturn(JSONObject jsonResult) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }
}
